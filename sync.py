from __future__ import print_function
import os

from numpy import interp
import pygame , sys , math, time
from pygame.locals import *

from audiosyncer import AudioSyncer

FREQUENCE_X_MIN = 95
FREQUENCE_X_STEP = 40
FREQUENCE_X_MAX = 655

SLIDER_Y_MIN = 150
SLIDER_Y_MAX = 570

HARD = 150
MEDIUM = 330

DEBUG_MODE = False

REQUIRED_TIME_AT_LEVEL = 10
DIFFICULTY = 3.0


def name():

    # define display surface
    W, H = 679, 486
    startTime = time.time()

    # define some colors
    COLOR_BG = (12, 32, 36, 255)
    COLOR_CURVE = (255,255, 255)
    COLOR_BEAT = (0, 0, 0, 0)
    NUMBER_OF_TONES = 24

    # initialise display
    pygame.init()
    CLOCK = pygame.time.Clock()
    DS = pygame.Surface((W, H))
    DS.fill(COLOR_BG)
    FPS = 1000.00
    MSPF = 0.001 / FPS

    # tone range
    position_range = [H - i * H // (NUMBER_OF_TONES - 1) for i in range(0, NUMBER_OF_TONES)]
    current_tone = NUMBER_OF_TONES // 2

    beat_dot_size = 10
    beat_dot_x = W - beat_dot_size

    dot_size = 3
    dot_x = 750
    dot_y = 500  # position_range[current_tone]
    sin_x = 0
    centerPosition = H//2

    freq = 0.005

    #render
    dot_size = 3
    dot_x = W - dot_size
    dot_y = int(H/2)  # position_range[current_tone]
    sin_x = 0

    S = AudioSyncer(freq_final_threshold=DIFFICULTY)

    pygame.init()
    pygame.mixer.pre_init(44100, -S._bits, 2)
    pygame.display.set_caption('Sync')
    screen = pygame.display.set_mode((1280, 720))
    screen.blit(DS, (0,100))

    numberOfChapters = 9
    currentChapter = 1
    chapterOpen = False
    file = ""
    score = 0
    listChapter = [None] * (numberOfChapters+2)
    oldChapters = 0
    intro = True
    nukeDisplay = False


    #SLIDER
    sliderX = 755
    sliderY = 0.5 * (SLIDER_Y_MIN + SLIDER_Y_MAX)

    #FREQUENCE
    frequenceX = 0.5 * (FREQUENCE_X_MIN + FREQUENCE_X_MAX)
    frequenceY = 75

    pressed = False
    backGround = pygame.image.load('ressourses/Sync_PanelBackground_V3_TopLayer.png')
    oscilCover = pygame.image.load('ressourses/Sync_PanelBackground_V3_MainWindow.png')
    nuke = pygame.image.load('ressourses/NukeScreen.png')
    nukeClicked = pygame.image.load('ressourses/NukeScreen_Press.png')
    btn = pygame.image.load("ressourses/Sync_SliderButton.png")
    btnF = pygame.image.load("ressourses/Sync_SliderButton_Top.png")
    yellowBtn = pygame.image.load("ressourses/Sync_LEDButtonYellow.png")
    greenBtn = pygame.image.load("ressourses/Sync_LEDButtonGreen.png")
    redBtn = pygame.image.load("ressourses/Sync_LEDButtonRed.png")
    bookBtn = pygame.image.load("ressourses/Sync_BookButton.png")
    screen.blit(btn, (sliderX, sliderY))
    screen.blit(btnF, (frequenceX, frequenceY))


    font1 = pygame.font.SysFont('Times New Roman', 20)
    font3 = pygame.font.SysFont('Times New Roman', 20)
    story = pygame.image.load("ressourses/StoryPaperPanel_01.png")
    label = font1.render("Score :", 1, (112, 87, 47))
    startTime = time.time()
    time_at_correct_level = 0

    while True:
        if (intro == True):
            i = 23
            screen.blit(story, (0, 0))
            with open("story/introduction") as f:
                for line in f:
                    line = line.rstrip()
                    storyRender = font1.render(line, True, (0, 0, 0))
                    screen.blit(storyRender, (375, 100 + i))
                    i += 23
        elif oldChapters == 0 and chapterOpen == False and currentChapter != numberOfChapters+2:
            score += 1
            sound = S.prepare_sound()
            sound.play()

        _events = []
        for evt in pygame.event.get():
            _events.append(evt)

            if evt.type == QUIT or evt.type == KEYDOWN and evt.key == K_ESCAPE:
                pygame.quit()
                return
            elif evt.type == MOUSEBUTTONDOWN and pressed == False and sliderX+150 > pygame.mouse.get_pos()[0] > sliderX and sliderY+150 > pygame.mouse.get_pos()[1] > sliderY:
                pressed = True
            elif evt.type == MOUSEBUTTONUP and 1005 < mousePos[0] < 1104 and 255 < mousePos[1] < 317 and chapterOpen == False:
                # old chapters
                if (listChapter[1] != None):
                    oldChapters = 1
            elif evt.type == MOUSEBUTTONUP and intro == True:
                intro = False
            elif evt.type == MOUSEBUTTONUP and currentChapter == numberOfChapters+1:
                if 371 < mousePos[0] < 908 and 97 < mousePos[1] < 626:
                    pygame.mixer.init()
                    nukeSound = 'ressourses/Kiwi-Nukebomb.mp3'
                    pygame.mixer.music.load(nukeSound)
                    pygame.mixer.music.play()
                    screen.blit(nukeClicked,(0,0))
                    pygame.display.flip()
                    pygame.time.wait(1000)
                    screen.fill((0,0,0))
                    pygame.time.wait(200)
                    pygame.mixer.stop()
                    pygame.quit()
                    return 1
                else:
                    S = AudioSyncer(freq_final_threshold = DIFFICULTY)
                    currentChapter = currentChapter + 1
            elif evt.type == MOUSEBUTTONUP and pressed == True:
                pressed = False
            elif evt.type == MOUSEBUTTONUP and oldChapters != 0:
                oldChapters = 0
            elif evt.type == pygame.KEYDOWN:
                if evt.key == pygame.K_LEFT  and chapterOpen != True and nukeDisplay != True and intro == False:
                    if oldChapters == 0 and (frequenceX - FREQUENCE_X_STEP) >= FREQUENCE_X_MIN:
                        frequenceX -= FREQUENCE_X_STEP

                        dot_y_old = dot_y

                        inc = -30
                        dot_y += inc
                        tmp1 = math.asin(math.sin(dot_y_old * sin_x * freq))
                        tmp2 = dot_y * sin_x * freq
                        
                        sin_x_inc_1 = (tmp1 - tmp2) / (dot_y * freq)

                        is_old_negative = math.cos(dot_y_old * sin_x * freq) < 0
                        is_new_1_negative = math.cos(dot_y * (sin_x + sin_x_inc_1) * freq) < 0

                        if is_old_negative == is_new_1_negative:
                            sin_x += sin_x_inc_1
                        else:
                            sin_x_inc_2 = (- tmp1 - tmp2 + math.pi) / (dot_y * freq)
                            sin_x += sin_x_inc_2
                            
                        screen.blit(backGround, (0,0))
                        screen.blit(bookBtn, (983,240))
                        screen.blit(btnF, (frequenceX, frequenceY))
                        screen.blit(btn, (sliderX, sliderY))
                        screen.blit(scoreL, (1012,150))
                    elif listChapter[oldChapters-1] != None:
                        oldChapters -= 1
                    elif listChapter[oldChapters-1] == None:
                        oldChapters = 0
                elif evt.key == K_RIGHT and chapterOpen != True and nukeDisplay != True and intro == False and intro == False:
                    if oldChapters == 0  and (frequenceX + FREQUENCE_X_STEP) <= FREQUENCE_X_MAX:
                        frequenceX += FREQUENCE_X_STEP

                        dot_y_old = dot_y

                        inc = 30
                        dot_y += inc
                        tmp1 = math.asin(math.sin(dot_y_old * sin_x * freq))
                        tmp2 = dot_y * sin_x * freq
                        
                        sin_x_inc_1 = (tmp1 - tmp2) / (dot_y * freq)

                        is_old_negative = math.cos(dot_y_old * sin_x * freq) < 0
                        is_new_1_negative = math.cos(dot_y * (sin_x + sin_x_inc_1) * freq) < 0

                        if is_old_negative == is_new_1_negative:
                            sin_x += sin_x_inc_1
                        else:
                            sin_x_inc_2 = (- tmp1 - tmp2 + math.pi) / (dot_y * freq)
                            sin_x += sin_x_inc_2

                        screen.blit(backGround, (0,0))
                        screen.blit(bookBtn, (983, 240))
                        screen.blit(btnF, (frequenceX, frequenceY))
                        screen.blit(btn, (sliderX, sliderY))
                        screen.blit(scoreL, (1012,150))

                    elif oldChapters != 0 and listChapter[oldChapters+1] != None:
                        oldChapters += 1
                    elif oldChapters != 0 and listChapter[oldChapters + 1] == None:
                        oldChapters = 0
                elif evt.key == K_UP and sliderY-30 > 100 and chapterOpen != True and chapterOpen != True and nukeDisplay != True  and oldChapters == 0 and intro == False:
                    sliderY -= 30
                elif evt.key == K_DOWN and sliderY-30 < 570 and chapterOpen != True and nukeDisplay != True and  oldChapters == 0 and intro == False:
                    sliderY += 30

            elif evt.type == MOUSEBUTTONUP:
                file = ""
                score = 0
                currentChapter += 1
                chapterOpen = False

        DS.fill(COLOR_BG, (W - 6, 0, dot_size * 2, H))
        pygame.draw.circle(DS, COLOR_CURVE, (dot_x, int(centerPosition + math.floor(math.sin(sin_x * dot_y * freq) * 200))),
                           dot_size, 0)
        DS.blit(DS, (0, 0), (6,0, W - 6, H))
        testTime = time.time()
        if testTime - startTime >= MSPF:
            startTime += MSPF
            sin_x += 0.1
            pygame.display.update()
            screen.blit(DS, (81, 148))
            screen.blit(oscilCover, (81,148))
            
        scoreL =""
        if S.freq_are_close_enough():
            scoreL = greenBtn
        elif score <= MEDIUM:
            scoreL = yellowBtn
        else:
            scoreL = redBtn
        screen.blit(scoreL, (1012,150))
        mousePos = pygame.mouse.get_pos()
        if pressed == True and 175 < mousePos[1] < 610:
            screen.blit(backGround, (0,0))
            screen.blit(bookBtn, (983, 240))
            screen.blit(btn, (sliderX, mousePos[1]-30))
            screen.blit(btnF, (frequenceX, frequenceY))
            screen.blit(scoreL, (1012,150))
            sliderY = mousePos[1]-30
        else:
            screen.blit(backGround, (0,0))
            screen.blit(bookBtn, (983, 240))
            screen.blit(btn, (sliderX, sliderY))
            screen.blit(btnF, (frequenceX, frequenceY))
            screen.blit(scoreL, (1012,150))

        coarse_frequence_reading = interp(
            frequenceX,
            [FREQUENCE_X_MIN+FREQUENCE_X_STEP, FREQUENCE_X_MAX-FREQUENCE_X_STEP],
            [S.min_freq, S.max_freq]
        )
        fine_frequence_reading = interp(
            sliderY,
            [SLIDER_Y_MIN, SLIDER_Y_MAX],
            [30, 0]
        )
        S.set_guess_freq_to(coarse_frequence_reading + fine_frequence_reading)


        #toogle story
        if time_at_correct_level > REQUIRED_TIME_AT_LEVEL and \
         currentChapter == numberOfChapters+1 and chapterOpen == False:
            nukeDisplay = True
            screen.blit(nuke, (0, 0))
        elif time_at_correct_level > REQUIRED_TIME_AT_LEVEL:
            time_at_correct_level = 0
            S = AudioSyncer(freq_final_threshold=DIFFICULTY)
            if score <= HARD:
                file = "story/message"+str(currentChapter)+"_full"
            elif score <= MEDIUM:
                file = "story/message" + str(currentChapter) + "_partial"
            else:
                file = "story/message" + str(currentChapter) + "_obstructed"
            listChapter[currentChapter] = file
            chapterOpen = True
        elif numberOfChapters >= currentChapter >= 0 and chapterOpen == False and score > 0 and S.freq_are_close_enough():
            time_at_correct_level += 1
        elif(chapterOpen == True):
            i = 23
            screen.blit(story, (0, 0))
            with open(file) as f:
                for line in f:
                    line = line.rstrip()
                    storyRender = font1.render(line, True, (0, 0, 0))
                    screen.blit(storyRender, (375, 100 + i))
                    i += 23
        elif oldChapters > 0:
            i = 23
            fileMem = listChapter[oldChapters]
            screen.blit(story, (0, 0))
            with open(fileMem) as f:
                for line in f:
                    line = line.rstrip()
                    storyRender = font1.render(line, True, (0, 0, 0))
                    screen.blit(storyRender, (375, 100 + i))
                    i += 23
            number = font3.render(str(oldChapters)+" / "+str(numberOfChapters), 1, (0,0,0))
            screen.blit(number, (500, 640))

        if DEBUG_MODE:
            print('Current fine tuning pos value %s, and freqX %s' % (sliderY, frequenceX))
            print('Coarse: %s. Fine: %s' % (coarse_frequence_reading, fine_frequence_reading))
            print('Freq target: %s. Current: %s' % (S.frequency_l, S.frequency_r))
            print(S.freq_are_close_enough())

if __name__ == "__main__":
    name()


