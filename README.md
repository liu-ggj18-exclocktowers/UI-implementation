## This is a main project repository

[Project page on GGJ site](https://globalgamejam.org/2018/games/sync)

## Running

You need [pygame](http://pygame.org) installed and have decent version of python (3.5 would do, but we only 'tested' it with 3.6)

After that, it should be as easy as just running the correct file

    $ python test.py
